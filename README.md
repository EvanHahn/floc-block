# FLoC Block

FLoC Block is Express middleware to block Google's FLoC tracking. Help [clean up the web](https://cleanuptheweb.org/).

## Quick start

First, install the package with `npm install floc-block`. Then, in an Express app:

```js
const flocBlock = require("floc-block");

// ...

app.use(flocBlock());
```

## How it works

This middleware sets the `Permissions-Policy` response header to `interest-cohort=()`, and that's it. The middleware takes no arguments.

If the header was already set earlier in your stack, it will be overwritten.

For finer-grained control over the `Permissions-Policy` header, set the header yourself or use the [permissions-policy npm package](https://github.com/pedro-gbf/permissions-policy).
