/// <reference types="node" />

import { IncomingMessage, ServerResponse } from "http";

declare function flocBlock(): (
  req: IncomingMessage,
  res: ServerResponse,
  next: () => unknown
) => void;

export = flocBlock;
