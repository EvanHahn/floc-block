module.exports = function flocBlock() {
  return function flocBlockMiddleware(_req, res, next) {
    res.setHeader("Permissions-Policy", "interest-cohort=()");
    next();
  };
};
