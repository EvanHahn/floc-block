const test = require("ava");
const connect = require("connect");
const supertest = require("supertest");

const flocBlock = require("./floc-block");

test("setting of the header", async (t) => {
  t.plan(0);

  const app = connect();
  app.use(flocBlock());
  app.use((req, res) => {
    res.end("Hello world!");
  });

  await supertest(app)
    .get("/")
    .expect("Permissions-Policy", "interest-cohort=()")
    .expect("Hello world!");
});

test("overriding an existing Permissions-Policy header", async (t) => {
  t.plan(0);

  const app = connect();
  app.use((req, res, next) => {
    res.setHeader("Permissions-Policy", "to be overridden");
    next();
  });
  app.use(flocBlock());
  app.use((req, res) => {
    res.end();
  });

  await supertest(app)
    .get("/")
    .expect("Permissions-Policy", "interest-cohort=()");
});

test("names its function and middleware", (t) => {
  t.is(flocBlock.name, "flocBlock");
  t.is(flocBlock().name, "flocBlockMiddleware");
});
